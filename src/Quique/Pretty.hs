{-# language PatternSynonyms #-}
{-# language ViewPatterns #-}
{-# language TypeSynonymInstances #-}
{-# language FlexibleInstances #-}
module Quique.Pretty (
  Pretty
, pretty
, pretty'
, printTree
) where

import CHR.Typed
import Data.List (intercalate)
import Data.Typeable
import Unbound.Generics.LocallyNameless

import Quique.Types
import Quique.Terms

class (Typeable p, Alpha p) => Pretty p where
  pretty :: Fresh m => p -> m String

pretty' :: Pretty p => p -> String
pretty' = runFreshM . pretty

instance Pretty t => Pretty (U t) where
  pretty (T t) = pretty t
  pretty v     = return (show v)

instance Typeable t => Pretty (Name t) where
  pretty = return . show

instance Pretty () where
  pretty _ = return ""

instance Pretty String where
  pretty = return

instance (Pretty a) => Pretty (Bind [TyVar] (MonoVarsInfo, [a], Ty)) where
  pretty b
    = do (vars, (info, cs, r)) <- unbind b
         pr  <- pretty r
         pcs <- mapM pretty cs
         let fs = "forall {" ++ intercalate " " (map (showV info) vars) ++ "}. "
             cr = if null cs
                     then ""
                     else "(" ++ intercalate ", " pcs ++ ") => "
         return $ fs ++ cr ++ pr
    where showV i v | v `elem` fullyMono    i = show v ++ "/m"
                    | v `elem` topLevelMono i = show v ++ "/t"
                    | otherwise               = show v ++ "/u"

instance Pretty a => Pretty (Bind [TyVar] ([a], Ty)) where
  pretty b
    = do (vars, (cs, r)) <- unbind b
         pr  <- pretty r
         pcs <- mapM pretty cs
         let fs = intercalate " " ("forall" : map show vars) ++ ". "
             cr = if null cs
                     then ""
                     else "(" ++ intercalate ", " pcs ++ ") => "
         return $ fs ++ cr ++ pr

instance Pretty (Con, ErrorContextIx) where
  pretty (c, n)
    = do pc <- pretty c
         return $ pc ++ " @ " ++ show n

instance Pretty Ty where
  pretty (Ty_Var v)
    = pretty v
  pretty (Ty_ForAll b)
    = pretty b
  pretty (Ty_List e)
    = do pe <- pretty e
         return $ "[" ++ pe ++ "]"
  pretty (Ty_Arrow s r)
    = do ps <- pretty s
         pr <- pretty r
         return $ mparens ps ++ " -> " ++ pr
  pretty (Ty_Con c (T as))
    = do pc  <- pretty c
         pas <- mapM pretty as
         return $ intercalate " " (pc : map mparens pas)
  pretty (Ty_Fam c (T as))
    = do pc  <- pretty c
         pas <- mapM pretty as
         return $ intercalate " " (('!' : pc) : map mparens pas)

instance Pretty GenTy where
  pretty (GenTy_Vars vs)
    = pretty vs

instance Pretty UseGuardedness where
  pretty AllMono  = return "."
  pretty UseGuard = return "*"

instance Pretty ResultGuardedness where
  pretty Result_Mono         = return "m"
  pretty Result_Unrestricted = return "u"

instance Pretty Con where
  pretty (Con_Eq t1 t2)
    = (\x y -> x ++ " ~ " ++ y) <$> pretty t1 <*> pretty t2
  pretty (Con_Inst t1 (T ug) (T _) t2)
    = (\x y -> x ++ " < " ++ y) <$> pretty t1 <*> pretty t2
  pretty (Con_Gen t1 (T n) (T _) t2)
    = (\x y -> x ++ " <g" ++ show n ++ " " ++ y) <$> pretty t1 <*> pretty t2
  pretty (Con_Class c (T as))
    = do pc <- pretty c
         pas <- mapM pretty as
         return $ intercalate " " (pc : map mparens pas)
  pretty (Con_Err m)
    = ("error " ++) <$> pretty m

printTree :: Pretty p => AnnExpr p -> String
printTree = unlines . runFreshM . printAST

printAST :: (Pretty p, Fresh m) => AnnExpr p -> m [String]
printAST (expr, ann)
  = do first:rest <- printASTExpr expr
       pann       <- pretty ann
       return $ (first ++ " : " ++ pann):rest

printASTExpr :: (Pretty p, Fresh m) => Expr p -> m [String]
printASTExpr (Var v)
  = return [show v]
printASTExpr (Ann e t)
  = do pe <- printAST e
       return $ ("ann " ++ pretty' t) : indent pe
printASTExpr (App e1 e2)
  = do pe1 <- printAST e1
       pe2 <- printAST e2
       return $ "app" : indent (pe1 ++ pe2)
printASTExpr (NApp f args)
  = do pf    <- printAST f
       pargs <- concat <$> mapM printAST args
       return $ "n-app" : indent (pf ++ pargs)
printASTExpr (Lam b)
  = do (x, e) <- unbind b
       pe <- printAST e
       return $ ("\\" ++ show x ++ ".") : indent pe
printASTExpr (AnnLam b)
  = do ((x, unembed -> t), e) <- unbind b
       pe <- printAST e
       pt <- pretty t
       return $ ("\\(" ++ show x ++ " :: " ++ pt ++ ").") : indent pe
{-
printASTExpr (TyApp e t)
  = do pe <- printAST e
       pt <- pretty t
       return $ ("tyapp " ++ pt) : indent pe
-}
printASTExpr (Int n)
  = return [show n]
printASTExpr (Str s)
  = return [show s]
printASTExpr (Let b)
  = do ((x, unembed -> e1), e2) <- unbind b
       pe1 <- printAST e1
       pe2 <- printAST e2
       return $ ("let " ++ show x ++ " =") : indent pe1 ++ "in" : indent pe2
printASTExpr (AnnLet b)
  = do ((x, unembed -> t, unembed -> e1), e2) <- unbind b
       pt  <- pretty t
       pe1 <- printAST e1
       pe2 <- printAST e2
       return $ ("let " ++ show x ++ " :: " ++ pt ++ " =") : indent pe1 ++ "in" : indent pe2
printASTExpr (Rule v args)
  = do pv <- pretty v
       pargs <- mapM printAST args
       return $ ("[bucket rule] " ++ pv) : indent (concat pargs) 

mparens :: String -> String
mparens ss@('[' : _) = ss
mparens ss@('(' : _) = ss
mparens s | ' ' `elem` s = "(" ++ s ++ ")"
          | otherwise    = s

indent :: [String] -> [String]
indent = map ("  " ++)
