{-# language PatternSynonyms #-}
module Main where

import CHR.Typed (pattern T)
import Control.Monad (forM, forM_, when)
import Text.Parsec (parse)
import System.Console.ANSI
import System.Environment (getArgs)
import Unbound.Generics.LocallyNameless (substs, runFreshM)

import Quique.Types
import Quique.Terms

import Quique.Parser
import Quique.Generation
import Quique.Pretty
import Quique.Solver

main :: IO ()
main
  = do cmdArgs <- getArgs
       let isShort = "short" `elem` cmdArgs
           isLoop  = "loop" `elem` cmdArgs
       code <- getContents
       case parse parseProgram "code" code of
         Left err ->
           do setSGR [SetColor Foreground Vivid Red]
              putStrLn "Parse error"
              setSGR [Reset]
              print err
         Right ast ->
           do let decls = typecheck isLoop ast
              forM_ decls $ \(v,e,c,einfo,s) -> do
                setSGR [SetColor Foreground Vivid Blue]
                putStr (show v)
                setSGR [Reset]
                if (not isShort) 
                then do
                  putStrLn ""
                  putStr (printTree e)
                  setSGR [SetColor Foreground Vivid Yellow]
                  putStrLn "Generated constraints:"
                  setSGR [Reset]
                  mapM_ prettyC c
                  mapM_ prettyErrorInfo einfo
                  setSGR [SetColor Foreground Vivid Green]
                  putStrLn "Solved AST:"
                  setSGR [Reset]
                  putStr (printTree (substs (substitution s) e))
                else do
                  putStr " => "
                  let (_, finalT) = e
                  putStrLn $ pretty' (substs (substitution s) finalT)
                let residualImportant = filter importantCC (residual s)
                when (not $ null residualImportant) $ do
                  setSGR [SetColor Foreground Vivid Magenta]
                  putStrLn "Residual constraints:"
                  setSGR [Reset]
                  mapM_ prettyCC residualImportant
                when (not isShort) $ putStrLn ""

typecheck :: Bool -> Program -> [(UVar, TExpr, ErrorConSet, [ErrorInfo], CCSolution)]
typecheck isLoop ast = runFreshM $
  do (axioms, decls) <- reconstructProgram ast
     forM decls $ \(v,e,c,einfo,ng) -> do
       let lp = if isLoop then LoopForTheBest else NoLoop
       sol <- solve' lp axioms ng c einfo
       return (v, e, c, einfo, sol)

prettyC :: (Con, ErrorContextIx) -> IO ()
prettyC (c,e)
  = do putStr "- "
       putStr (pretty' c)
       putStr " @ "
       putStrLn (show e)

prettyErrorInfo :: ErrorInfo -> IO ()
prettyErrorInfo (c1,c2,m)
  = do putStr "- "
       putStr (show c1)
       putStr " ==>> "
       putStr (show c2)
       putStr " \""
       putStr m
       putStrLn "\""

importantCC :: CC -> Bool
importantCC (Ok (T _) _ _)    = True
importantCC (Err (T _) _ _ _) = True
importantCC _                 = False

prettyCC :: CC -> IO ()
prettyCC (Ok (T c) _ _)
  = do putStr "- "
       putStrLn (pretty' c)
prettyCC (Err (T c) _ _ (T Nothing))
  = do putStr "- ERROR: "
       putStrLn (pretty' c)
prettyCC (Err (T c) _ _ (T (Just msg)))
  = do putStr "- ERROR: "
       putStrLn (pretty' c)
       putStr "  because "
       putStrLn msg
prettyCC _ = return ()
