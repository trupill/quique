# The Quique experimental compiler

## User-specified axioms

Meta-variables in the CHR level are prefixed by `$`. For example:

    axiom Eq [$a] <=> Eq $a;
    axiom Ord $a => Eq $a;

Note that the system makes no check at this moment to guarantee confluence or termination.

## Constraint generation with buckets

The syntax for a custom type rule with buckets can be seen in this example:

    rule map fn lst
      :: bucket "Fn. and list do not coincide" {
           bucket "1st arg. should be a function" {
             argument fn, $fn < $0 -> $1
           },
           bucket "2nd. arg should be a list" {
             argument lst, $lst < [$2]
           },
           $0 ~ $2,
           $return ~ [$1]
         };

In particular:
- You need to give a name to each of the arguments to the function.
- You need to include at some point the constraints for each argument using `argument`.
- You refer to the type of argument `a` by using the metavariable `$a`.
- Similarly, `$return` refers to the return type of the expression.
- You can use other metavariables with numbers: `$0`, `$1` and so on.